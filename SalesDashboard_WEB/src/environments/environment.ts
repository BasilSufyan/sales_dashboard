// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mock:false,
  firebase:{
    apiKey: "AIzaSyAVEAASc0Jt_VLtdHCo_AdL39gPqTD--Bk",
    authDomain: "prayagtimber-d12ac.firebaseapp.com",
    databaseURL: "https://prayagtimber-d12ac.firebaseio.com",
    projectId: "prayagtimber-d12ac",
    storageBucket: "prayagtimber-d12ac.appspot.com",
    messagingSenderId: "660156401055",
    appId: "1:660156401055:web:dbcbe450617079e0aa69aa",
    measurementId: "G-VCDLPCE56F"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
