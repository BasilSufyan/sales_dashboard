export class Item{
    id?:string;
    Name?:string;
    DgCode:number;
    FNS:string;
    LastUpdatedDate?:string;
    LastUpdatedStatus?:boolean; // false = added, true = subtracted
    OrderNumber?:number;
    Quantity?:number;
}