export interface InventoryList {
    name?: string;
    dgcode: number;
    fns: string;
    quantity: number;
    lastupdateddate?:string;
    lastupdatedstatus?:boolean;
  }