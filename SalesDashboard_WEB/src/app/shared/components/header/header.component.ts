import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FirestoreService } from 'src/app/services/firestore.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() toggleSidebarEvent:EventEmitter<any> = new EventEmitter<any>();
  constructor(private firebaseSvc:FirestoreService,private router:Router) { }

  ngOnInit(): void {
  }

  toggleSidebar(){
    this.toggleSidebarEvent.emit();
  }

  logout(){
    sessionStorage.clear();
    this.firebaseSvc.logout()
    .then( ()=>{
      this.router.navigate(['login']);
    })
    .catch(ex =>{
      console.error(ex);
      this.router.navigate(['login']);
    });
  }
}
