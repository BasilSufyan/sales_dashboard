import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../app/layouts/default/default.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { AddProductComponent } from './modules/add-product/add-product.component';
import { AddBillComponent } from './modules/add-bill/add-bill.component';
import {InventoryComponent} from './modules/inventory/inventory.component';
import { SetupInventoryComponent } from './modules/setupInventory/setup-inventory.component';
import { LoginComponent } from './modules/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { AuthChildGuard } from './guards/auth-child.guard';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'home',
    component: DefaultComponent,
    canActivate: [AuthGuard],
    canActivateChild:[AuthChildGuard],
    children: [
      {
        path: '',
        component: InventoryComponent
      },
      {
        path: 'inventory',
        component: InventoryComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'addproducts',
        component: AddProductComponent
      },
      {
        path: 'addbill',
        component: AddBillComponent
      },
      {
        path: 'setupInventory',
        component: SetupInventoryComponent
      },
    ]
  },
  {path: '**', component: LoginComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
