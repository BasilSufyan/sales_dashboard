import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  fileType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  constructor(private httpSvc: HttpClient) {}

  getInventoryList(): Observable<any> {
    let url = '';
    return this.httpSvc.get(url);
  }

  addProduct(data): Observable<any> {
    let url = '';
    return this.httpSvc.post(url, data);
  }

  updateInventory(data): Observable<any> {
    let url = '';
    return this.httpSvc.post(url, data);
  }

  exportToExcel(data: any[], filename: string) {

    filename += new Date().toISOString();

    let filteredData = [];
    for(let x of data){
      let obj = {};
      obj['FNS'] = x['FNS'];
      obj['DgCode'] = x['DgCode'];
      obj['Quantity'] = x['Quantity'];
      obj['LastUpdated Date'] = x['LastUpdatedDate'];
      filteredData.push(obj);
    }
    

    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(filteredData);
    const wb: XLSX.WorkBook = { Sheets: { data: ws }, SheetNames: ['data'] };
    const exportBuffer: any = XLSX.write(wb, {
      bookType: 'xlsx',
      type: 'array',
    });
    const datatoexport: Blob = new Blob([exportBuffer], { type: this.fileType });
    FileSaver.saveAs(datatoexport, filename + '.xlsx');
  }
}
