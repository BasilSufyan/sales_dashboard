import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Item } from '../shared/interface/item';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  itemsCollection: AngularFirestoreCollection<Item>;
  items: Observable<Item[]>;
  itemDoc: AngularFirestoreDocument<Item>;

  constructor(private readonly afs: AngularFirestore,private  afAuth:  AngularFireAuth) {
    this.itemsCollection = afs.collection<Item>('store80');
    //this.items = this.itemsCollection.valueChanges();
    this.items = this.itemsCollection.snapshotChanges().pipe(map(changes => {

      return changes.map( (a:any) => {
        const item = a.payload.doc.data() as Item; 
        item.id = a.payload.doc.id;
        return item;
      })}
    ));
  }

  getItems() {
    return this.items;
  }

  addItem(item: Item) {
    this.itemsCollection.add(item);
  }

  updateItem(item: Item) {
    console.log(item);
    this.itemDoc = this.afs.doc(`store80/${item.id}`);
    return this.itemDoc.update(item);
  }

  deleteItem(item: Item) {
    console.log("Item to delete",item.id);
    this.itemDoc = this.afs.doc(`store80/${item.id}`);
    this.itemDoc.delete();
  }

  find(fns,dgcode){
    return this.afs.collection<Item>('store80', ref => ref.where('DgCode','==',dgcode).where('FNS', '==', fns))
    .snapshotChanges().pipe(map(changes => {
      return changes.map( (a:any) => {
        const item = a.payload.doc.data() as Item; 
        item.id = a.payload.doc.id;
        return item;
      })}
    ));    
  }

  logout(){
    console.log('logout');
    return this.afAuth.signOut();
  }

  async login(email: string, password: string) {
    var result = await this.afAuth.signInWithEmailAndPassword(email, password)
    return result;
  }

  
  
}