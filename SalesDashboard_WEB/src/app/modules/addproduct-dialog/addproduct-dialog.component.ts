import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FirestoreService } from 'src/app/services/firestore.service';
import { Item } from 'src/app/shared/interface/item';

@Component({
  selector: 'app-addproduct-dialog',
  templateUrl: './addproduct-dialog.component.html',
  styleUrls: ['./addproduct-dialog.component.scss']
})
export class AddproductDialogComponent implements OnInit {

  view:boolean;
  productObject:any;
  error_addproduct:string="";

  newEntryform:FormGroup;
  DgCode:FormControl;
  FNS:FormControl;
  Quantity:FormControl;
  ExistingQuantity:FormControl;

  orderEntryform:FormGroup;
  ordernumber:FormControl;
  date:FormControl;
  error_orderpage: string = "";
  id: FormControl;


  constructor( public dialogRef: MatDialogRef<AddproductDialogComponent>, private firestore:FirestoreService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    if(this.data.mode == 'order'){
      this.view = false;
      this.createFormControls_order();
      this.createForm_order();
    }
    else{
      this.view = true;
      this.createFormControls();
      this.createForm();
    }
  }
  createForm_order() {
    this.orderEntryform = new FormGroup({
      ordernumber:this.ordernumber,
      date:this.date
    });
  }

  createForm() {
   this.newEntryform = new FormGroup({
     DgCode:this.DgCode,
     FNS:this.FNS,
     Quantity:this.Quantity,
     ExistingQuantity:this.ExistingQuantity,
     id:this.id
   });
  }

  createFormControls() {
    this.DgCode = new FormControl('',[Validators.required,Validators.pattern(/^[0-9]+$/)]);
    this.FNS = new FormControl('',Validators.required);  
    this.Quantity = new FormControl('',[Validators.required, Validators.pattern(/^[0-9]+$/)]);
    this.ExistingQuantity = new FormControl('');
    this.id = new FormControl('');
  }

  onSubmit_newEntry() {
    if (this.newEntryform.valid) {
      console.log("Form Submitted!");
      this.FNS.setValue(this.FNS.value.toUpperCase());
      this.firestore.find(this.FNS.value,this.DgCode.value)
      .subscribe( (item) =>{
        console.log(item);
        if(item && item.length>0){
          this.ExistingQuantity.setValue(item[0].Quantity)
          this.id.setValue(item[0].id);
          this.dialogRef.close(this.newEntryform.value);
          this.newEntryform.reset();
        }    
        else{
          alert('DGCode and FNS combination not found');
        }  
      })
      
    }
    else{
      this.error_addproduct = "Enter Correct Data";
    }
  }
  
  createFormControls_order() {
    this.ordernumber = new FormControl('',[Validators.required,Validators.pattern(/^[0-9]+$/)]);
    this.date = new FormControl('',Validators.required);
  }

  onSubmit_orderEntry(){
    if (this.orderEntryform.valid) {
      console.log("Form Submitted!");
      this.dialogRef.close(this.orderEntryform.value);
      this.orderEntryform.reset();
    }
    else{
      this.error_orderpage = "Enter Correct Data";
    }
  }  
  
  onClose(){
    this.dialogRef.close(false);
  }

}
