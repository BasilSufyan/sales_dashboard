import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddproductDialogComponent } from '../addproduct-dialog/addproduct-dialog.component';
import { InventoryList } from '../../shared/interface/inventory';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { CommonService } from '../../services/common.service';
import { Item } from 'src/app/shared/interface/item';
import { MatSort } from '@angular/material/sort';
import { FirestoreService } from 'src/app/services/firestore.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})


export class AddProductComponent implements OnInit,AfterViewInit {

  ELEMENT_DATA: AddItems[] = [];
  
  dataSource = new MatTableDataSource<AddItems>(this.ELEMENT_DATA);
  displayedColumns = ['DgCode','FNS','ExistingQuantity','Quantity','action'];
  orderNumber:number;
  OrderDate:string;
  total_quantity:number = 0;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static:false}) sort:MatSort;
  count: number;
  
  constructor(public dialog: MatDialog,private commonSvc:CommonService, private firestore:FirestoreService) { }

  ngOnInit(): void {
    this.count = 0;
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openDialog_addrow() {
    const dialogRef = this.dialog.open(AddproductDialogComponent,{
      width: '350px',
      data:{
        mode:'row'
      }      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result:`,result);
      if(result){
      //result['id']=this.count;
      this.count++;
      this.ELEMENT_DATA.push(result);      
      this.dataSource.data = this.ELEMENT_DATA;
      }

      this.ELEMENT_DATA.map(x =>{
        var quantity = x.Quantity;
        this.total_quantity = this.total_quantity +  x.Quantity;
      });
    });
  }

  openDialog_order() {
    const dialogRef = this.dialog.open(AddproductDialogComponent,{
      width: '350px',
      data:{
        mode:'order'
      }      
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result:`,result);
      this.orderNumber = result.ordernumber;
      this.OrderDate = result.date;
    });
  }

  deleterow(element){
    //this.ELEMENT_DATA.(element);
    this.ELEMENT_DATA = this.ELEMENT_DATA.filter( x => {
      return x['id'] != element.id
    });
    this.dataSource.data = this.ELEMENT_DATA;
    console.log(element);
  }

  submitData(){
    console.log(this.ELEMENT_DATA);
    let count = 0;
    this.ELEMENT_DATA.forEach( item =>{
      let updatedCount = item.ExistingQuantity+item.Quantity;
      item.Quantity = updatedCount;
      item.LastUpdatedStatus = false;
      this.firestore.updateItem(item).then(resp=>{
        count++;
        if(count == this.ELEMENT_DATA.length){
          console.log("data updated successfully");
          alert("data updated successfully");
          this.ELEMENT_DATA = [];
          this.dataSource.data = this.ELEMENT_DATA;
          this.total_quantity = 0;
        }
      })
      .catch(e=>{
        alert("error while updating data");
        console.error("error while updating data",e.message);
      })
    })
    // this.commonSvc.addProduct(this.ELEMENT_DATA)
    // .subscribe(data =>{

    // });    
  }

}

class AddItems extends Item { public ExistingQuantity:number};