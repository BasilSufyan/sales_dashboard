import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Item } from 'src/app/shared/interface/item';
import { FirestoreService } from 'src/app/services/firestore.service';

@Component({
  selector: 'app-setup-inventory',
  templateUrl: './setup-inventory.component.html',
  styleUrls: ['./setup-inventory.component.scss']
})
export class SetupInventoryComponent implements OnInit {

  newEntryform:FormGroup;
  DgCode:FormControl;
  FNS:FormControl;
  Quantity:FormControl;
  Name:FormControl;

  error_addproduct:string="";

  constructor(private firestoreSvc : FirestoreService) { }

  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
  }

  createForm() {
    this.newEntryform = new FormGroup({
      DgCode:this.DgCode,
      FNS:this.FNS,
      Quantity:this.Quantity,
      Name:this.Name
    });
   }
   createFormControls() {
    this.DgCode = new FormControl('',[Validators.required,Validators.pattern(/^[0-9]+$/)]);
    this.FNS = new FormControl('',Validators.required);  
    this.Quantity = new FormControl('',[Validators.required, Validators.pattern(/^[0-9]+$/)]);
    this.Name = new FormControl('');
  }

  onSubmit_newEntry() {
    if (this.newEntryform.valid) {
      console.log("Form Submitted!");
      this.error_addproduct = "";
      this.FNS.setValue(this.FNS.value.toUpperCase());
      console.log(this.newEntryform.value);
      this.addItem(this.newEntryform.value);
      this.newEntryform.reset();
    }
    else{
      this.error_addproduct = "Enter Correct Data";
    }
  }

  addItem(item:Item){
    let today = new Date();
    let date = today.getDate()+'/'+(today.getMonth()+1)+'/'+today.getFullYear();
    item.LastUpdatedDate = date;
    item.LastUpdatedStatus = false;
    this.firestoreSvc.addItem(item);
  }
}
