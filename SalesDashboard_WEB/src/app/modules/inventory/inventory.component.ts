import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { InventoryList } from '../../shared/interface/inventory';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { InventryDialogComponent } from '../inventry-dialog/inventry-dialog.component';
import { CommonService } from 'src/app/services/common.service';
import { environment } from 'src/environments/environment';
import {FirestoreService} from 'src/app/services/firestore.service';
import { Item } from 'src/app/shared/interface/item';



@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.scss']
})
export class InventoryComponent implements OnInit,AfterViewInit {
   ELEMENT_DATA: Item[] = [
    {DgCode: 1, Name: 'White', FNS: "HG", Quantity: 10, LastUpdatedDate:"27/09/2020",LastUpdatedStatus:true},
    {DgCode: 202, Name: 'Black', FNS: "HG", Quantity: 10, LastUpdatedDate:"27/09/2020",LastUpdatedStatus:false},
    {DgCode: 101, Name: 'White', FNS: "HG", Quantity: 10, LastUpdatedDate:"27/09/2020",LastUpdatedStatus:false},
    {DgCode: 102, Name: 'White', FNS: "HG", Quantity: 10, LastUpdatedDate:"27/09/2020",LastUpdatedStatus:true},
    {DgCode: 103, Name: 'White', FNS: "HG", Quantity: 10, LastUpdatedDate:"27/09/2020",LastUpdatedStatus:false},
    {DgCode: 104, Name: 'White', FNS: "HG", Quantity: 10, LastUpdatedDate:"27/09/2020",LastUpdatedStatus:true},
    {DgCode: 105, Name: 'White', FNS: "HG", Quantity: 10, LastUpdatedDate:"27/09/2020",LastUpdatedStatus:true},
    {DgCode: 106, Name: 'White', FNS: "HG", Quantity: 10, LastUpdatedDate:"27/09/2020",LastUpdatedStatus:true}
  ];
  dataSource = new MatTableDataSource<Item>();
  displayedColumns = ['FNS','DgCode','Quantity','LastUpdatedDate','LastUpdatedStatus','action'];
  filter: any = [
    {'column':'DgCode', 'value':''},
    {'column':'FNS', 'value':''},
    {'column':'LastUpdatedDate', 'value':''},
  ];

  constructor(public dialog: MatDialog,private commonSvc:CommonService, private firestore:FirestoreService) { }

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort:MatSort;

  ngOnInit(): void {
    if (environment.mock) {
      this.dataSource.data = this.ELEMENT_DATA;
    }
    else {
      // this.commonSvc.getInventoryList()
      //   .subscribe(data => {
      //     console.log(data);
      //     this.dataSource.data = data;
      //   });
      this.firestore.getItems().subscribe((items:Item[]) =>{
        console.log(items);
        this.dataSource.data = items;
      })
    }
  
    this.dataSource.filterPredicate = this.filterPredicateFunction.bind(this);
  }

  ngAfterViewInit():void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;   
  }

  filterData(event){
    let filtervalue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filtervalue;
  }

  filterPredicateFunction(data:Item, filter:any):boolean{
    
    for(let i=0; i<filter.length;i++){
      if(filter[i].value){
        let filtsthisfilter = false;
        filtsthisfilter = data[filter[i].column].toString().toLowerCase().indexOf(filter[i].value.toLowerCase()) > -1;
        if(!filtsthisfilter)
          return false;
      }      
    }

    return true;


    // if(data.DgCode.toString().indexOf(filter)>-1){
    //   return true;
    // }
    // else if(data.Name.toUpperCase().indexOf(filter.toUpperCase()) >-1){
    //   return true;
    // }
    // else if(data.FNS.toUpperCase().indexOf(filter.toUpperCase())>-1){
    //   return true;
    // }
    // else if(data.LastUpdatedDate.toUpperCase() == filter.toUpperCase()){
    //   return true;
    // }
    // else 
    //   return false;

  }

  inputClick(event){
    event.stopPropagation();
  }

  applyfilter(event,sourceId){
    let filterValue = (event.target as HTMLInputElement).value;
    this.filter = this.filter.map((row)=>{
      if(row.column == sourceId)
        row.value = filterValue;
      
        return row;
    });

    this.dataSource.filter = this.filter;
  }

  openEntry(element:Item){
    const dialogRef = this.dialog.open(InventryDialogComponent,{
      width: '450px',
      data:element  
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log("dialog result",result);
        element.Quantity = element.Quantity - result.Quantitysold;
        element.LastUpdatedStatus = true;
        let today = new Date();
        let date = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
        element.LastUpdatedDate = date;
       
        this.firestore.updateItem(element).then(resp=>{
          console.log("data updated successfully");
          alert("data updated successfully");
        })
        .catch(e=>{
          alert("error while updating data");
          console.error("error while updating data",e.message);
        })
      }

    });
  }

  deleterow(element){
    if(element){
      if(window.confirm('Are you sure you want to delete')){
        this.firestore.deleteItem(element);
      }
    }
  }
  exportdata(){
    console.log('export data');
    this.commonSvc.exportToExcel(this.dataSource.data, 'inventory');
  }
}
