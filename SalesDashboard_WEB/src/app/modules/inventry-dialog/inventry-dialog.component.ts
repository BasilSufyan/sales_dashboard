import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-inventry-dialog',
  templateUrl: './inventry-dialog.component.html',
  styleUrls: ['./inventry-dialog.component.scss']
})
export class InventryDialogComponent implements OnInit {

  
  Entryform:FormGroup;
  DgCode:FormControl;
  FNS:FormControl;
  Quantity:FormControl;
  Quantitysold: FormControl;
  error: string = '';

  constructor(public dialogRef: MatDialogRef<InventryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    console.log(this.data);
    this.createFormControls(this.data);
    this.createForm();
  }
  createFormControls(basevalue) {
    this.DgCode = new FormControl(basevalue.DgCode,[Validators.required]);
    this.FNS = new FormControl(basevalue.FNS,Validators.required);  
    this.Quantity = new FormControl(basevalue.Quantity,[Validators.required]);
    this.Quantitysold = new FormControl('',[Validators.required, Validators.pattern(/^[0-9]+$/)]);
  }
  createForm() {
    this.Entryform = new FormGroup({
      DgCode:this.DgCode,
      FNS:this.FNS,
      Quantity:this.Quantity,
      Quantitysold:this.Quantitysold
    });
  }
  onSubmit(){
    if (this.Entryform.valid && (this.Quantitysold.value <= this.Quantity.value)) {
      console.log("Form Submitted!");
      this.dialogRef.close(this.Entryform.value);
      this.Entryform.reset();
    }
    else{
      this.error = "Enter Correct Data";
    }
  }
  
  onClose(){
    this.dialogRef.close(false);
  }
}
