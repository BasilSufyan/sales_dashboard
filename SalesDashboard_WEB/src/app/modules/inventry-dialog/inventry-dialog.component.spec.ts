import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventryDialogComponent } from './inventry-dialog.component';

describe('InventryDialogComponent', () => {
  let component: InventryDialogComponent;
  let fixture: ComponentFixture<InventryDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventryDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
