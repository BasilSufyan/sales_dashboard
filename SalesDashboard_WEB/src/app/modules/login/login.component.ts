import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FirestoreService } from 'src/app/services/firestore.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup;
  email:FormControl;
  password:FormControl;

  constructor(private router:Router, private firebase: FirestoreService) { }

  ngOnInit(): void {
    this.createFormControls();
    this.createForm();
  }

  createFormControls() {
    this.email = new FormControl('',[Validators.required,Validators.pattern(/\S+@\S+\.\S+/)]);
    this.password = new FormControl('',Validators.required);      
  }

  createForm() {
    this.loginForm = new FormGroup({
      email:this.email,
      password:this.password     
    });
  }

  onSubmit(){
    if (this.loginForm.valid) {
      this.firebase.login(this.email.value,this.password.value)
      .then(resp =>{
        console.log(resp);
        sessionStorage.setItem('token',resp.user.refreshToken);
        setTimeout( ()=>{
          sessionStorage.clear();
        },60*60*1000);
        this.router.navigate(['home/inventory']);
      })
      .catch(ex =>{
        console.error(ex);
        alert("Invalid Credentials");
      })
    }
    else{
      alert("Enter correct credentials");
    }
  }  

}
