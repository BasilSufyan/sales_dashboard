import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {

  sideBarOpened=true;
  constructor() { }

  ngOnInit(): void {
    // if small screen, close sidebar
    if(screen.width < 768){
      this.sideBarOpened = false;
    }
  }

  toggleSideBar(event){
    this.sideBarOpened = !this.sideBarOpened;
  }

  closeSideBar(){
    this.sideBarOpened = false;
  }

}
