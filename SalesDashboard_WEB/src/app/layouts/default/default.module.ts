import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultComponent } from '../default/default.component';
import { DashboardComponent } from '../../modules/dashboard/dashboard.component';
import { InventoryComponent } from '../../modules/inventory/inventory.component';
import { AddProductComponent } from '../../modules/add-product/add-product.component';
import { AddBillComponent } from '../../modules/add-bill/add-bill.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { AddproductDialogComponent } from 'src/app/modules/addproduct-dialog/addproduct-dialog.component';
import { InventryDialogComponent } from '../../modules/inventry-dialog/inventry-dialog.component';
import { DateAdapter } from '@angular/material/core';
import { SetupInventoryComponent } from 'src/app/modules/setupInventory/setup-inventory.component';



@NgModule({
  declarations: [
    DefaultComponent,
    DashboardComponent,
    InventoryComponent,
    AddProductComponent,
    AddBillComponent, 
    AddproductDialogComponent,
    InventryDialogComponent,
    SetupInventoryComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,   
  ],
})
export class DefaultModule { }
