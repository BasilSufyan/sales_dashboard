import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private userService: UserService,private router:Router) {};

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
     if(this.userService.isLoggedIn()){
       return true;
     }else{
      alert("session expired. Redirecting to login");
       this.router.navigate(['login']);
        return false;
     }
  }
  
}

@Injectable({
  providedIn: 'root'
})
export class UserService {
  isLoggedIn(): boolean {
    let token = sessionStorage.getItem('token');
    if(token){
      return true;
    }
    return false; // Switch to `false` to make OnlyLoggedInUsersGuard work
  }
}
