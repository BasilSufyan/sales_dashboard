﻿USE [INVENTORY]
GO
DROP PROCEDURE IF EXISTS [dbo].[cpSetClassType]
GO
  
Create Procedure [dbo].[cpSetClassType]
	@ClassTypeID int = NULL OUTPUT,
	@Name varchar(40) = NULL OUTPUT,
	@UpdTS timestamp = NULL OUTPUT  
As  
Begin  

    /**********************************************************************************
    Stored Procedure:cpSetClassType
    
    Created:		
    Description:	This Procedure will insert/update a record in the table ClassType
    
    Date				Who	Description
    ------------------------------------------------------------------------------------
    
    ************************************************************************************/  
      
    Declare @ErrorCode int  
    Declare @RowNumber int  
  
    Set NoCount On  
  
  
  
        If @ClassTypeID is NULL
        Begin  
            Insert Into ClassType  
		(Name,
		 CreatedDate)  
            Values  
		(
		 @Name,
         GetDate()

		 )  
  
            Select @UpdTS = @@DBTS, @ErrorCode = @@ERROR, @RowNumber = @@ROWCOUNT  
              
        End  
    Else  
        Begin  
            Update ClassType  
            Set	
		Name = @Name
            Where	ClassTypeID = @ClassTypeID
            And	UpdTS = @UpdTS  
  
            Select @UpdTS = @@DBTS, @ErrorCode = @@ERROR, @RowNumber = @@ROWCOUNT  
            If @ErrorCode <> 0 Or @RowNumber <> 1  
                Begin  
                    RaisError('Error[ClassType]: This record was changed by other users', 11, 1) 
                    Return  
                End  
        End  
    
    
  
      
    Return 0  
End  
GO
