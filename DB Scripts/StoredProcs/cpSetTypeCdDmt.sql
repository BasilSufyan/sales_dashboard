﻿USE [INVENTORY]
GO
DROP PROCEDURE IF EXISTS [dbo].[cpSetTypeCd]
GO
  
Create Procedure [dbo].[cpSetTypeCd]
	@TypeCdID INT = NULL OUTPUT,
	@ClassTypeID INT = NULL OUTPUT,
	@ObjectCd VARCHAR(10) = NULL OUTPUT,
	@StatusCd TINYINT = NULL OUTPUT,
	@Descr VARCHAR(80) = NULL OUTPUT,
	@UpdTS timestamp = NULL OUTPUT  
As  
Begin  

    /**********************************************************************************
    Stored Procedure:cpSetTypeCdDmt
    Created:		
    Description:	This Procedure will insert/update a record in the table TypeCdDmt

    Date				Who	Description
    ------------------------------------------------------------------------------------

    ************************************************************************************/  
      
    Declare @ErrorCode int  
    Declare @RowNumber int  
  
    Set NoCount On  

    If @TypeCdID is NULL
    Begin  
            Insert Into TypeCdDmt  
		(ClassTypeID,
		 ObjectCd,
         CreatedDate,
		 StatusCd,
		 Descr)  
            Values  
		(@ClassTypeID,
		 @ObjectCd,
         GetDate(),
		 @StatusCd,
		 @Descr)  
  
            Select @UpdTS = @@DBTS, @ErrorCode = @@ERROR, @RowNumber = @@ROWCOUNT  
              
        End  
    Else  
        Begin  
            Update TypeCdDmt  
            Set	ClassTypeID = @ClassTypeID,
		ObjectCd = @ObjectCd,
		StatusCd = @StatusCd,
		Descr = @Descr  
            Where	TypeCdID = @TypeCdID
            And	UpdTS = @UpdTS  
  
            Select @UpdTS = @@DBTS, @ErrorCode = @@ERROR, @RowNumber = @@ROWCOUNT  
            If @ErrorCode <> 0 Or @RowNumber <> 1  
                Begin  
                    RaisError('Error[TypeCdDmt]: This record was changed by other users', 11, 1) 
                    Return  
                End  
        End  
    
    
  
      
    Return 0  
End  
GO
