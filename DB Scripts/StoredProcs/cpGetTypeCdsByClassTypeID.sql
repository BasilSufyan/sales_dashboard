﻿USE [INVENTORY]
GO
DROP PROCEDURE IF EXISTS [dbo].[cpGetTypeCdsByClassTypeID]
GO

Create Procedure [dbo].[cpGetTypeCdsByClassTypeID] 	@ClassTypeID int = NULL OUTPUTAS/**********************************************************************************
Stored Procedure:   GetTypeCdsByClassTypeID
Created:	Dec 15 2002  1:35:55:763AM    
Description :       This procedure will get multiple records from the TypeCdDmt table

Date                                Who     Description
------------------------------------------------------------------------------------

************************************************************************************/
Set nocount on	SELECT TypeCdID,ClassTypeID,ObjectCd,StatusCd,Descr	FROM TypeCdDmt 	WHERE ClassTypeID = @ClassTypeID and StatusCd  = 1
GO
