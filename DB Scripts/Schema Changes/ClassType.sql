﻿/************************************************************************
Database:	Inventory
Date	:	
 Date	 Who		Description
------------------------------------------------------------------------------------

************************************************************************************/

If NOT EXISTS ( Select * from sys.tables where Name='ClassType')
Begin 

	CREATE TABLE [dbo].[ClassType]
	(		ClassTypeID		INT NOT NULL IDENTITY(1,1),
			Name			VARCHAR(10) NOT NULL,
			CreatedDate		DateTime NOT NULL,
			[UpdTS]			TIMESTAMP NOT NULL,
			CONSTRAINT [conClassType1PK] PRIMARY KEY CLUSTERED 
			(
			[ClassTypeID] ASC
			)
	)
	
End
GO




