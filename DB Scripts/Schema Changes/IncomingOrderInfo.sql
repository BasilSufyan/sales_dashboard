﻿/************************************************************************
Database:	Inventory
Date	:	
 Date	 Who		Description
------------------------------------------------------------------------------------

************************************************************************************/

If NOT EXISTS ( Select * from sys.tables where Name='IncomingOrderInfo')
Begin 

	CREATE TABLE [dbo].[IncomingOrderInfo]
	(		IncomingOrderInfoID		INT NOT NULL IDENTITY(1,1),
			OrderDate			DateTime NOT NULL,
			[UpdTS]				TIMESTAMP NOT NULL
	)
	
End
GO




