﻿/************************************************************************
Database:	Inventory
Date	:	
 Date	 Who		Description
------------------------------------------------------------------------------------

************************************************************************************/

If NOT EXISTS ( Select * from sys.tables where Name='TypeCdDmt')
Begin 

	CREATE TABLE [dbo].[TypeCdDmt]
	(		TypeCdID		INT NOT NULL IDENTITY(1,1),
			ClassTypeID		INT NOT NULL,
			ObjectCd		VARCHAR(10) NOT NULL,
			Descr			VARCHAR(80) NOT NULL,
			CreatedDate		DateTime NOT NULL,
			StatusCd		TINYINT NOT NULL, 
			[UpdTS]			TIMESTAMP NOT NULL,
			CONSTRAINT [conTypeCdDmt1PK] PRIMARY KEY CLUSTERED 
			(
			[TypeCdID] ASC
			)
	)
			
	
End
GO

ALTER TABLE [dbo].[TypeCdDmt]  WITH CHECK ADD  CONSTRAINT [conTypeDmtT1Fk] FOREIGN KEY([ClassTypeID])
REFERENCES [dbo].[ClassType] ([ClassTypeID])




