﻿/************************************************************************
Database:	Inventory
Date	:	
 Date	 Who		Description
------------------------------------------------------------------------------------

************************************************************************************/

If NOT EXISTS ( Select * from sys.tables where Name='StockStatus')
Begin 

	CREATE TABLE [dbo].[StockStatus]
	(		StockStatusID		INT NOT NULL IDENTITY(1,1),
			FNS							INT NOT NULL,
			DGCode						INT NOT NULL,
			Quantity					INT NOT NULL,
			CreatedDate					DateTime NOT NULL,
			LastUpdatedStatus			VARCHAR(10) NULL,
			[UpdTS]						TIMESTAMP NOT NULL
	)
	
End
GO




