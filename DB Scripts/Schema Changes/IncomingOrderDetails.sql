﻿/************************************************************************
Database:	Inventory
Date	:	
 Date	 Who		Description
------------------------------------------------------------------------------------

************************************************************************************/

If NOT EXISTS ( Select * from sys.tables where Name='IncomingOrderDetails')
Begin 

	CREATE TABLE [dbo].[IncomingOrderDetails]
	(		IncomingOrderDetailID		INT NOT NULL IDENTITY(1,1),
			IncomingOrderInfoID			INT NOT NULL,
			FNS							INT NOT NULL,
			DGCode						INT NOT NULL,
			Quantity					INT NOT NULL,
			CreatedDate					DateTime NOT NULL,
			[UpdTS]						TIMESTAMP NOT NULL
	)
	
End
GO




